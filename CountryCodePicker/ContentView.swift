//
//  ContentView.swift
//  CountryCodePicker
//
//  Created by Leadconsultant on 11/12/19.
//  Copyright © 2019 Leadconsultant. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    
    var countriesList = ["United States", "India", "Namibia"]
    
    @State var selectedCountry = "Namibia"
    var body: some View {
        Picker(selection: $selectedCountry, label: Text("")) {
            ForEach(0 ..< countriesList.count) {
                Text(self.countriesList[$0]).tag($0)
                
            }
        }.labelsHidden()
            .foregroundColor(Color.white)
            .background( Color("AccentColorLight") )
            .cornerRadius(15)
            .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
